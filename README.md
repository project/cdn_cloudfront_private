# Amazon CloudFront private files integration

This is an API module to assist with [serving private/protected content](http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/PrivateContent.html)
through Amazon CloudFront.

Access may be controlled by signed URLs (query string parameters) or a
signed cookie.

This module will require some configuration to work; it is not plug-and-play.
Administrators must configure a signing key and specify the key's ID in the
module configuration. There are then three main paths to integration, listed
from simplest to most complex:

### Scheme-based URL signing

In your site's `services.yml` file, specify stream wrappers for which to sign
generated URLs. E.g.:

```yaml
parameters:
  cdn_cloudfront_private.schemes: ['s3']
```

In addition, you will likely need to configure the stream wrapper adapter to
write URLs directly to CloudFront. E.g. if the above stream wrapper is added
with `flysystem_s3`, the configuration would be marked public and include a
`cname` for your CloudFront distribution in front of the S3 bucket:

```php
$schemes['s3'] = [
  'driver' => 's3',
  'config' => [
    'key' => getenv('S3_ACCESS_KEY'),
    'secret' => getenv('S3_SECRET_KEY'),
    'bucket' => getenv('S3_BUCKET'),
    'endpoint' => getenv('S3_ENDPOINT') ?: 'https://s3.amazonaws.com',
    'public' => TRUE,
    'protocol' => 'https',
    'cname' => 'your-distribution.cloudfront.net',
  ],
  'name' => 'S3',
  'cache' => TRUE,
  'serve_js' => FALSE,
  'serve_css' => FALSE,
];
```

***A major downside of this integration method*** is that all pages with signed
URLs will not be cacheable; this is due to a limitation in Drupal Core's file
URL generation API. If you can isolate out the instances in which the signed
URLs will be generated and your policy is conducive to some sort of caching,
consider the next option.

### Signed `GeneratedUrl`

A typical use case for signed URLs is protected content/media; in these contexts
the generation of URLs is likely more predictable than delegating entirely to
the core URL generation API. If you control the contexts in which these URLs are
generated, you may use `Signer::getSignedGeneratedUrl()` to obtain a
`GeneratedUrl` with applicable cache metadata. Combined with a lazy builder or
BigPipe, this may be an effective hedge against busting your cache with signed
URLs.

### Signed cookies without individually signed URLs

If your use case allows or requires (e.g., with unpredictable or unknown file
URLs from streaming video HLS manifests) you may wish to sign a cookie for the
host and path of your protected CloudFront distribution, and allow the browser
to pass this cookie as authentication for your content. In this case, call
`Signer::setSignedCookies()` and `Signer::unsetSignedCookies()` per your
business rules.

## What should the CloudFront distribution point to?

Using CloudFront as a content-protecting CDN does not necessarily require using
S3; in fact, any HTTP(S) backend can serve as an origin. Pointing CloudFront
at the S3 bucket which is registered in Drupal as a stream wrapper allows you to
avoid any origin pulls through Drupal, which can be slow. This may not be an
option, however, if you need image styles generated on request. If your files
are accessible via Flysystem, be sure to protect them from direct access.

### Further protecting content when using Flysystem

A sample use case would be to use
[Flysystem](https://drupal.org/project/flysystem)
to create a new "protected" stream wrapper to in effect provide a souped-up
version of the private files functionality in Drupal core. Use an integration
method above to allow access to privileged users, and then restrict direct
access of those files to only Cloudfront using Custom Origin Headers and an
implementation of `hook_file_download()`:

```php
/**
 * Implements hook_file_download().
 */
function mymodule_file_download($uri) {
  $scheme = \Drupal::service('file_system')->uriScheme($uri);
  if ($scheme == 'protected') {
    $request = \Drupal::request();
    if ($request->headers->get('CDN-Token') != getenv('CDN_TOKEN')) {
      return -1;
    }
  }
  return NULL;
}
```

## Copyright and license.

Copyright 2016-2020 Brad Jones LLC and contributors. GPL-2.
