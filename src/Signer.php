<?php declare(strict_types=1);

namespace Drupal\cdn_cloudfront_private;

use Aws\CloudFront\CloudFrontClient;
use Drupal\cdn_cloudfront_private\Exception\CloudFrontPrivateException;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\key\KeyRepository;

class Signer implements SignerInterface {

  /**
   * Default TTL. 1 hour.
   */
  const TTL = 60 * 60;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Key repository.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * Service parameters.
   *
   * @var array
   */
  protected $options;

  /**
   * The Cloudfront client.
   *
   * @var \Aws\CloudFront\CloudFrontClient
   */
  protected $client;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  public function __construct(array $options, ConfigFactory $configFactory, KeyRepository $keyRepository, TimeInterface $time) {
    $this->options = $options;
    $this->configFactory = $configFactory;
    $this->keyRepository = $keyRepository;
    $this->time = $time;
    $this->client = new CloudFrontClient([
      // Not effective, but required.
      'region' => 'us-west-2',
      'version' => '2016-09-29',
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public static function getDefaultPolicyStatement(?int $customTtl = NULL, ?string $customResource = NULL): array {
    return [
      'Resource' => $customResource ?? 'https://*',
      'Condition' => [
        'DateLessThan' => [
          'AWS:EpochTime' => \Drupal::time()->getRequestTime() + ($customTtl ?? self::TTL),
        ],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function generate(string $uri): ?string {
    $scheme = StreamWrapperManager::getScheme($uri);
    if (!in_array($scheme, $this->options['schemes'])) {
      return NULL;
    }
    // At the moment, bubbleable metadata for a generated URL is only available
    // via a GeneratedUrl, e.g. as returned by calling Url::toString(TRUE).
    // It is recommended that site implementations requiring signed URLs make
    // them cacheable and/or build them via lazy builders, and generate the URLs
    // using the above procedure rather than through a method that calls
    // file_create_url().
    \Drupal::service('page_cache_kill_switch')->trigger();
    return $this->sign($uri);
  }

  /**
   * {@inheritDoc}
   */
  public function sign(string $url = NULL, ?array $policy = NULL, ?int $ttl = NULL): string {
    assert(!($policy && $ttl), 'Passing both a policy and TTL is unsupported.');
    $config = $this->configFactory->get('cdn_cloudfront_private.config');
    $pem = $this->keyRepository->getKey($config->get('key'));
    if ($pem->getKeyProvider()->getPluginDefinition()['storage_method'] == 'file') {
      $configuration = $pem->getKeyProvider()->getConfiguration();
      $privateKey = $configuration['file_location'];
    }
    else {
      $privateKey = $pem->getKeyValue();
    }
    $opts = [
      'private_key' => $privateKey,
      'key_pair_id' => $config->get('key_pair_id'),
      'url' => $url,
      'policy' => $policy
        ? json_encode(['Statement' => [$policy]], JSON_UNESCAPED_SLASHES)
        : NULL,
      'expires' => $policy
        ? NULL
        : $this->time->getRequestTime() + ($ttl ?? self::TTL),
    ];
    return $this->client->getSignedUrl(array_filter($opts));
  }

  /**
   * {@inheritDoc}
   */
  public function setSignedCookies(?array $policy = NULL, ?bool $secure = NULL, string $path = ''): void {
    if (is_null($policy)) {
      $policy = self::getDefaultPolicyStatement();
    }
    if (is_null($secure)) {
      $secure = $this->options['secure'];
    }
    if (!$path) {
      $secure = $this->options['path'];
    }
    $config = $this->configFactory->get('cdn_cloudfront_private.config');
    $pem = $this->keyRepository->getKey($config->get('key'));
    if ($pem->getKeyProvider()->getPluginDefinition()['storage_method'] == 'file') {
      $configuration = $pem->getKeyProvider()->getConfiguration();
      $privateKey = $configuration['file_location'];
    }
    else {
      $privateKey = $pem->getKeyValue();
    }
    $opts = [
      'private_key' => $privateKey,
      'key_pair_id' => $config->get('key_pair_id'),
      'policy' => json_encode(['Statement' => [$policy]], JSON_UNESCAPED_SLASHES),
    ];
    if (empty($policy['Resource'])) {
      throw new CloudFrontPrivateException('AWS signed cookies require a resource to be specified.');
    }
    $cookies = $this->client->getSignedCookie($opts);
    $domain = $config->get('domain');
    foreach ($cookies as $c => $d) {
      setrawcookie($c, $d, NULL, $path, $domain, $secure, TRUE);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function unsetSignedCookies(?bool $secure = NULL, string $path = ''): void {
    $cookies = [
      'CloudFront-Policy',
      'CloudFront-Key-Pair-Id',
      'CloudFront-Signature',
    ];
    if (is_null($secure)) {
      $secure = $this->options['secure'];
    }
    if (is_null($path)) {
      $secure = $this->options['path'];
    }
    $domain = $this->configFactory
      ->get('cdn_cloudfront_private.config')->get('domain');
    foreach ($cookies as $cookieName) {
      // Delete.
      setcookie($cookieName, '', 0, $path, $domain, $secure);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getSignedGeneratedUrl(string $uri, ?int $customTtl = NULL, ?string $customResource = NULL): GeneratedUrl {
    $policy = NULL;
    if ($customResource) {
      $policy = self::getDefaultPolicyStatement($customTtl, $customResource);
    }
    return (new GeneratedUrl())
      ->setGeneratedUrl(
        $this->sign($uri, $policy, $customTtl)
      )
      ->setCacheMaxAge($customTtl ?? self::TTL);
  }

}
