<?php declare(strict_types=1);

namespace Drupal\cdn_cloudfront_private\Exception;

class CloudFrontPrivateException extends \Exception {}
