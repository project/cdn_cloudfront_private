<?php declare(strict_types=1);

namespace Drupal\cdn_cloudfront_private;

use Drupal\Core\GeneratedUrl;

interface SignerInterface {

  /**
   * Returns a default policy statement.
   *
   * Static so that extending implementors may use this as a starting point
   * for their own policy, to be passed to ::getSignedUrl().
   *
   * @param int|NULL $customTtl
   *   Custom TTL to apply.
   * @param string|NULL $customResource
   *   Custom resource to apply.
   *
   * @return array
   *   A default policy statement.
   */
  public static function getDefaultPolicyStatement(?int $customTtl = NULL, ?string $customResource = NULL): array;

  /**
   * Generate a signed, external URL for a URI; primarily for integration with
   * hook_file_url_alter().
   *
   * @see \cdn_cloudfront_private_file_url_alter()
   *
   * @param string $uri
   *   The URI for which to generate a signed URL.
   *
   * @return string|null
   *   The signed URL or NULL if we aren't signing.
   */
  public function generate(string $uri): ?string;

  /**
   * Sign a URL.
   *
   * @param string $url
   *   The URI to sign.
   * @param array|NULL $policy
   *   The policy statement, or NULL to use the default.
   * @param int|NULL $ttl
   *   TTL, in seconds, for the signature validity. Allows for signing with a
   *   "canned" policy.
   *
   * @return string
   *   The URL, signed.
   *
   * @throws \Drupal\cdn_cloudfront_private\Exception\CloudFrontPrivateException
   *
   * @see https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-creating-signed-url-canned-policy.html
   */
  public function sign(string $url = NULL, ?array $policy = NULL, ?int $ttl = NULL): string;

  /**
   * Set the signed cookie.
   *
   * @param array|NULL $policy
   *   The policy statement, or NULL to use the default.
   * @param bool|NULL $secure
   *   Whether to mark the cookie as secure.
   * @param string $path
   *   Path to apply cookie.
   *
   * @throws \Drupal\cdn_cloudfront_private\Exception\CloudFrontPrivateException
   */
  public function setSignedCookies(?array $policy = NULL, ?bool $secure = NULL, string $path = ''): void;

  /**
   * Unset signed cookies.
   *
   * @param bool|null $secure
   *   Secure flag.
   * @param string $path
   *   Path.
   */
  public function unsetSignedCookies(?bool $secure = NULL, string $path = ''): void;

  /**
   * Get a signed generated URL with the default policy. We don't (yet) support
   * passed policies as we don't (yet) have logic to map all the possible
   * cache metadata, but you can override the TTL.
   *
   * @param string $uri
   *   URI to sign.
   * @param int|NULL $customTtl
   *   Custom TTL to apply.
   * @param string|NULL $customResource
   *   Custom resource value to apply.
   *
   * @return \Drupal\Core\GeneratedUrl
   */
  public function getSignedGeneratedUrl(string $uri, ?int $customTtl = NULL, ?string $customResource = NULL): GeneratedUrl;

}
